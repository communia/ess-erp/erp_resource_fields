<?php

namespace Drupal\erp_resource_fields\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Sort handler for sorting by search score.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("available_quantity")
 */
class AvailableQuantity extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    
    $this->ensureMyTable();
    //$this->query->addField(
    // TODO add as SQL calc field:
    // https://api.drupal.org/api/drupal/core%21modules%21comment%21src%21Plugin%21views%21sort%21Thread.php/class/Thread/8.2.x
      
    //$this->query->addOrderBy(NULL, "({$this->tableAlias}.{$this->realField} - {$this->tableAlias}.{$this->table}__reserved_stock )", $this->options['order'], "bla");//$this->tableAlias . '_' . $this->field);
    //commerce_product_variation_field_data_commerce_product__variations.
    $this->query->addOrderBy(NULL, "commerce_product_variation_field_data_commerce_product__variations.reserved_stock", $this->options['order'], $this->tableAlias . '_' . $this->field);
    return;
    //
    // Check to see if the search filter/argument added 'score' to the table.
    // Our filter stores it as $handler->search_score -- and we also
    // need to check its relationship to make sure that we're using the same
		// one or obviously this won't work.
    foreach (array(
      'filter',
      'argument',
    ) as $type) {
      foreach ($this->view->{$type} as $handler) {
        if (isset($handler->search_score) && $handler->relationship == $this->relationship) {
          $this->query
            ->addOrderBy(NULL, NULL, $this->options['order'], $handler->search_score);
          $this->tableAlias = $handler->tableAlias;
          return;
        }
      }
    }

    // Do nothing if there is no filter/argument in place. There is no way
    // to sort on scores.
  }

}
