<?php

namespace Drupal\erp_resource_fields\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\views\Views;
use Drupal\erp_resource_fields\ResourceViewsFieldBase;

/**
 * A handler to provide a field with processed value of specified item.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("execution_production_views_field")
 */
class ExecutionProductionViewsField extends ResourceViewsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();

    $production_order_execution = $this->addProductionExecutionFields();

    $formula = "( COALESCE(". $production_order_execution['planned_quantity_alias_table'] . ".planned_quantity_value, 0) - COALESCE(" . $production_order_execution['executed_quantity_alias_table'] .".executed_quantity_value, 0 ))";

    $this->field_alias = $this->query->addField(NULL, $formula, $this->tableAlias . '_production_exec_stock_up_calc_' .$this->field, ['function' => 'sum' ] );
  }
}
