<?php

namespace Drupal\erp_resource_fields\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\views\Views;
use Drupal\erp_resource_fields\ResourceViewsFieldBase;

/**
 * A handler to provide a field with processed value of specified item.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("processed_purchase_views_field")
 */
class ProcessedPurchaseViewsField extends ResourceViewsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function query() {
    parent::query();

    $purchase_order_pendings = $this->addPendingFields("erp_purchase_order");

    $formula = "(" . $purchase_order_pendings['quantity_alias_table'] . ".quantity + COALESCE(" . $purchase_order_pendings['processed_alias_table'] . ".processed_value,0 ))";

    $this->field_alias = $this->query->addField(NULL, $formula, $this->tableAlias . '_purchase_oi_stock_up_calc_' .$this->field, ['function' => 'sum' ] );
  }
}
