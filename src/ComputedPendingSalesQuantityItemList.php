<?php
namespace Drupal\erp_resource_fields;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ComputedPendingSalesQuantityItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $resource = $this->getEntity();
    if ($resource->bundle() !== "erp_resource"){
      return;
    }

    $sales_outgoing = 0;
    $query = \Drupal::entityQuery('commerce_order_item') 
		  ->condition('type', 'erp_sales')
      ->condition('purchased_entity', $resource->id())
      ->condition('order_id.entity.state',['draft' ,'completed' ,'canceled'], 'NOT IN')
      ;
	  $result = $query->execute();
		if (count($result) > 0){
			$order_items_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
      $resources = $order_items_storage->loadMultiple($result);
			foreach($resources as $res){
				$sales_outgoing += $res->quantity->value - $res->processed->value;
			}
    }
    $computed_values = [ $sales_outgoing ]; 
    foreach ($computed_values as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }


}

