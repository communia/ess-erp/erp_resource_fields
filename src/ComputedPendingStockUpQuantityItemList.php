<?php
namespace Drupal\erp_resource_fields;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ComputedPendingStockUpQuantityItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $resource = $this->getEntity();
    if ($resource->bundle() !== "erp_resource"){
      return;
    }

    $production_order_incoming = 0;

    $query = \Drupal::entityQuery('commerce_order')
      ->condition('type', 'erp_production_order')
      ->condition('state',['draft' ,'completed' ,'canceled'], 'NOT IN')
      ->condition('target_product', $resource->id());

	  $result = $query->execute();
		if (count($result) > 0){
			$order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
      $resources = $order_storage->loadMultiple($result);
			foreach($resources as $res){
				$production_order_incoming += $res->planned_quantity->value - $res->executed_quantity->value;
			}
    }

    $purchase_incoming = 0;
    $query = \Drupal::entityQuery('commerce_order_item') 
		  ->condition('type', 'erp_purchase')
      ->condition('purchased_entity', $resource->id())
      ->condition('order_id.entity.state',['draft' ,'completed' ,'canceled'], 'NOT IN')
      ;
	  $result = $query->execute();
		if (count($result) > 0){
			$order_items_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
      $resources = $order_items_storage->loadMultiple($result);
			foreach($resources as $res){
				$purchase_incoming += $res->quantity->value + $res->processed->value;
			}
    }
    $computed_values = [ ($production_order_incoming) + ($purchase_incoming) ]; 
    foreach ($computed_values as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }


}

