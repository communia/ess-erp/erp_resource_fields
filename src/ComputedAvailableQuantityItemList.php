<?php
namespace Drupal\erp_resource_fields;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ComputedAvailableQuantityItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $resource = $this->getEntity();
    if ($resource->bundle() !== "erp_resource"){
      return;
    }
    $computed_values = [ $resource->quantity->value - $resource->reserved_stock->value ]; //#TODO MODELRC move to dedicated field (not user created... ) <do whatever computation is needed to get the field values>;
    foreach ($computed_values as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }


}

