It enables two flavoured fields, the ones to be used in the entity as a field, and other to be used in views.

# Entity Field:
It is computed in real time via php so it cannot be used in views to sort or filter results.
- ComputedAvailableQuantityItemList
- ComputedNeededQuantityItemList
- ComputedPendingSalesQuantityItemList
- ComputedPendingStockUpQuantityItemList

# Views field.

The other provided fields are views fields to simplify views, as it can be obtained in the same way in UI but are
useful to make views simplier. which calculate values to prepare variables to pass to a math
expression that will be evaluated by a https://www.drupal.org/project/views_simple_math_field field.

In all cases will act from a product variation, it will fetch the order items using it and calculate these
views fields:
- Processed Production : Quantity - processed.
- Processed Purchase :Quantity + processed (as a note take care that processed will be inverted in
  this case (we mark as it will increase stock when creating transaction marking order item as not substracting,
  leading to a negative value in processed)).
- Processed Sales : Quantity - processed.
- Execution Production: Planned + executed.


