<?php
namespace Drupal\erp_resource_fields;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ComputedNeededQuantityItemList extends FieldItemList /*implements ContainerFactoryPluginInterface*/ {

  #TODO add ERP Query Service as https://drupal.stackexchange.com/a/233751
  use ComputedItemListTrait;

  /*1*
   * {@inheritdoc}
   */
  protected function computeValue() {
    $resource = $this->getEntity();
    if ($resource->bundle() !== "erp_resource"){
      return;
    }
    $computed_values = [$resource->pending_sales_quantity->value - $resource->available_quantity->value + $resource->minimum_stock_level->value - $resource->pending_stock_up_quantity->value ];
    foreach ($computed_values as $delta => $value) {
      $this->list[$delta] = $this->createItem($delta, $value);
    }
  }

}

