<?php

namespace Drupal\erp_resource_fields;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\views\Views;

/**
 * A Base handler to provide a ERP resource computed fields.
 *
 */
class ResourceViewsFieldBase extends NumericField {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // If a relationship is set, we must use the alias it provides.
    if (!empty($this->relationship)) {
      $this->tableAlias = $this->relationship;
    }
     // If no relationship, then use the alias of the base table.
    else{
      $this->tableAlias = $this->query->ensureTable($this->view->storage->get('base_table'));
    }
  }

  /**
   * Adds production execution fields, namely planned_quantity and executed_quantity values, to query
   *
   * @returns array
   *   a keyed array with "planned_alias_table" and "execution_alias_table" for product
   *
   */
  protected function addProductionExecutionFields($type = NULL){
    //reserved stock relatinship: (to make present the field reserved stock (to
    // sum after)
    //quantity is included in commerce_order_item table as it comes from
    //stock... So join a table for field is not needed

    $configuration = [
      'table' => 'commerce_order__target_product',
      'field' => 'target_product_target_id',
      'left_table' => 'commerce_product_variation_field_data',
      'left_field' => 'variation_id',
      'operator' => '='
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $target_product_relationship_alias = $configuration['table'] . "_target_" . $this->table;
    $target_alias_table = $this->query->addRelationship($target_product_relationship_alias, $join, 'commerce_product_variation_field_data',$this->relationship);
    //quantity relationship: (to make present the field quantity (to sum after)
    $configuration = [
      'table' => 'commerce_order',
      'field' => 'order_id',
      'left_table' => $target_product_relationship_alias,
      'left_field' => 'entity_id',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $order_relationship_alias = $configuration['table'] . "_prod_order_" . $this->table;
    $order_alias_table = $this->query->addRelationship($order_relationship_alias, $join, 'commerce_product_variation_field_data', $target_relationship_alias);

    // order planned relationship..
    $configuration = [
      'table' => 'commerce_order__planned_quantity',
      'field' => 'entity_id',
      'left_table' => 'commerce_order_field_data',
      'left_field' => 'order_id',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $planned_relationship_alias = $configuration['table'] . "_planned_" . $this->table;
    $planned_alias_table = $this->query->addRelationship($planned_relationship_alias, $join, 'commerce_product_variation_field_data', $order_relationship_alias);

    // order executed relatinship:
    $configuration = [
      'table' => 'commerce_order__executed_quantity',
      'field' => 'entity_id',
      'left_table' => 'commerce_order_field_data',
      'left_field' => 'order_id',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $executed_relationship_alias = $configuration['table'] . "_executed_" . $this->table;
    $executed_alias_table = $this->query->addRelationship($executed_relationship_alias, $join, 'commerce_product_variation_field_data',$order_relationship_alias);


    // TODO until https://www.drupal.org/project/drupal/issues/3049408 lands...
    // we need full field and cannot use the alias returned...

    $planned = $this->query->addField($planned_relationship_alias, "planned_quantity_value", $planned_relationship_alias . '_planned_' . $this->field);
    $executed = $this->query->addField($executed_relationship_alias, "executed_quantity_value", $executed_relationship_alias . '_executed_' . $this->field);

    // TODO until https://www.drupal.org/project/drupal/issues/3049408 lands...
    // we need full field and cannot use the alias returned... : so table.field
    // must be used.
    return [
      "planned_quantity_alias_table" => $planned_alias_table,
      "executed_quantity_alias_table" => $executed_alias_table,
    ];
  }


  
  /**
   * Adds pending fields, namely quantity and processed values, to query
   *
   * @var string $type
   *   will fetch only the order items of this type.
   *
   * @returns array
   *   a keyed array with "quantity_alias_table" and "processed_alias_table" for this order item.
   *
   */
  protected function addPendingFields($type = NULL){
    //reserved stock relatinship: (to make present the field reserved stock (to
    // sum after)
    //quantity is included in commerce_order_item table as it comes from
    //stock... So join a table for field is not needed

    $configuration = [
      'table' => 'commerce_order_item',
      'field' => 'purchased_entity',
      'left_table' => 'commerce_product_variation_field_data',
      'left_field' => 'variation_id',
      'operator' => '='
    ];
    if ($type){
      $configuration['extra'][] = [
        'field' => 'type',
        'value' => $type,
      ];
    }

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $purchase_oi_quantity_relationship_alias = $configuration['table'] . "_q_" . $this->table;
    $quantity_alias_table = $this->query->addRelationship($purchase_oi_quantity_relationship_alias, $join, 'commerce_product_variation_field_data',$this->relationship);
    //quantity relationship: (to make present the field quantity (to sum after)
    $configuration = [
      'table' => 'commerce_order_item__processed',
      'field' => 'entity_id',
      'left_table' => $purchase_oi_quantity_relationship_alias,
      'left_field' => 'order_item_id',
      'operator' => '='
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $purchase_oi_processed_relationship_alias = $configuration['table'] . "_processed_ois_" . $this->table;
    $processed_alias_table = $this->query->addRelationship($purchase_oi_processed_relationship_alias, $join, 'commerce_product_variation_field_data', $purchase_oi_quantity_relationship_alias);


    // Calculate:
    // We are just using this field to be present in the query to allow post
    // formula carry on, we sum here because we don't want duplicates , but the
    // sum is something we will not use (posterior sum gets the
    // raw value obtained from query not the summed here.
    $quantity = $this->query->addField($purchase_oi_quantity_relationship_alias, "quantity", $purchase_oi_quantity_relationship_alias . '_q_' . $this->field, ['function' => 'sum' ] );
    $processed = $this->query->addField($purchase_oi_processed_relationship_alias, "processed_value", $purchase_oi_processed_relationship_alias . '_processed_' .$this->field, ['function' => 'sum' ] );

    // TODO until https://www.drupal.org/project/drupal/issues/3049408 lands...
    // // we need full field and cannot use the alias returned... : so table.
    // field must be used...
    //$formula = "( " . $quantity . " - " . $reserved_stock . " )";
    return [
      "quantity_alias_table" => $quantity_alias_table,
      "processed_alias_table" => $processed_alias_table,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
  public function render(ResultRow $values) {
    // Return a random text, here you can include your custom logic.
    // Include any namespace required to call the method required to generate
    // the desired output.
    $random = new Random();
    return $random->name();
  }
   */

}
